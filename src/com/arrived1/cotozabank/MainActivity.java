package com.arrived1.cotozabank;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.google.ads.AdRequest;
import com.google.ads.AdView;


public class MainActivity extends Activity {
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		BankList bankList = new BankList(this);
		
		addAdView();
		addListenerOnKeyEnter(bankList);
		addListenerOnButtonReset();
	}
	
	private void addAdView() {
		AdView ad = (AdView) findViewById(R.id.adView);
		ad.loadAd(new AdRequest());
	}
	
	private void addListenerOnKeyEnter(BankList bankList) {
		EditText accountNumber = (EditText) findViewById(R.id.accountNumber2);
		accountNumber.setOnKeyListener(new OnKeyListenerEnter(this, bankList));
	}
	
	private void addListenerOnButtonReset() {
		final Button button = (Button) findViewById(R.id.buttonReset);
		button.setOnClickListener(new ButtonOnClickListenerReset(this));
	}
	
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		MenuItem item = menu.findItem(R.id.menu_settings);

	    if(item == null)
	    	return true;

	    item.setOnMenuItemClickListener(new MenuOnMenuIteamClickListenerAboutProgram(this));
		return true;
	}

}
