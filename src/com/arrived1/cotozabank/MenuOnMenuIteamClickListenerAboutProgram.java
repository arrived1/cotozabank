package com.arrived1.cotozabank;

import android.app.Activity;
import android.content.Intent;
import android.view.MenuItem;

public class MenuOnMenuIteamClickListenerAboutProgram implements MenuItem.OnMenuItemClickListener {
	private Activity actv;
	
	public MenuOnMenuIteamClickListenerAboutProgram(Activity activity_) {
		this.actv = activity_;
	}
	
	@Override
	public boolean onMenuItemClick(MenuItem item) {
		actv.startActivity(new Intent(actv, AboutProgramActivity.class));
    	return true;
	}

}
