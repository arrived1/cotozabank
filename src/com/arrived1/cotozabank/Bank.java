package com.arrived1.cotozabank;


public class Bank {
	
	private int id = 0;
	private String name = "";
	
	public Bank(int id_, String name_) {
		this.id = id_;
		this.name = name_;
	}
	
	public Bank(String line) {
		String bankId = line.substring(0, 4);
		this.id = Integer.parseInt(bankId); 
		this.name = line.substring(5);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}
}
