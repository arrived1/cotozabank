package com.arrived1.cotozabank;

import android.app.Activity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ButtonOnClickListenerReset implements View.OnClickListener {
	private Activity actv;
	
	public ButtonOnClickListenerReset(Activity activity_) {
		this.actv = activity_;
	}
	
	@Override
	public void onClick(View v) {
		EditText bankName = (EditText)actv.findViewById(R.id.accountNumber2);
    	bankName.setText("");
    	
    	TextView bankNamePrinter = (TextView)actv.findViewById(R.id.bankName);
    	bankNamePrinter.setText("");
	}
}


